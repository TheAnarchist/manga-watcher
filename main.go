package main

import (
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/TheAnarchist/manga-watcher/pkg"
)

func init() {
	pkg.InitLogger()
}

func main() {
	repo := pkg.CreateRepository()
	smtpClient := pkg.CreateSMTPClient()

	users := repo.FindUsersByWeekDay()
	for _, user := range users {
		if user.HasExpiredToken() {
			user.UpdateToken()
		}

		source := pkg.CreateSourceApi(user.ClientId)
		newChapters := source.GetNewChapters(user.Token)

		shikiUser := source.WhoAmI(user.Token)
		smtpClient.Send(user.Email, shikiUser.Nickname, newChapters)
	}
}
