package pkg

import (
	"testing"
)

type TitleTestCase struct {
	urlPath  string
	expected string
}

func TestGetCorrectTitle(t *testing.T) {
	for num, testCase := range getTestGetCorrectTitleData() {
		if actual := GetCorrectTitle(testCase.urlPath); actual != testCase.expected {
			t.Fatalf("Url path %d should be %v, got %v\n", num, testCase.expected, actual)
		}
	}
}

func getTestGetCorrectTitleData() []TitleTestCase {
	return []TitleTestCase{
		{urlPath: "/triniti__semero_magov", expected: "triniti__semero_magov"},
		{urlPath: "/one_punch_man__A1bc88e", expected: "one_punch_man"},
		{urlPath: "/fairy_tail__100_years_quest", expected: "fairy_tail__100_years_quest"},
		{urlPath: "/boruto__A112d3e7", expected: "boruto"},
		{urlPath: "/my_hero_academia__A1be6f8", expected: "my_hero_academia"},
	}
}
