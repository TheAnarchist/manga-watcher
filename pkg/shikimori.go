package pkg

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	BaseUrl           = "https://shikimori.one/"
	BaseAPIUrl        = BaseUrl + "api"
	TokenUrl          = BaseUrl + "oauth/token"
	ThrottlingTimeout = 200 * time.Millisecond
	UserAgent         = "TheAnarchist App"
)

type ShikimoriUser struct {
	Id       int64  `json:"id"`
	Nickname string `json:"nickname"`
}

type ShikimoriInterface interface {
	RefreshToken(oldToken Token) Token
	WhoAmI(token Token) ShikimoriUser
	GetNewChapters(token Token) []NewChapters
}

type Shikimori struct {
	clientID     string
	clientSecret string
	userID       int64
}

func (api Shikimori) RefreshToken(oldToken Token) Token {
	form := url.Values{
		"grant_type":    {"refresh_token"},
		"client_id":     {api.clientID},
		"client_secret": {api.clientSecret},
		"refresh_token": {oldToken.RefreshToken},
	}

	resp, err := http.PostForm(TokenUrl, form)
	if err != nil {
		log.WithError(err).Error("Couldn't refresh token")
	}

	var newToken Token
	decodeErr := json.NewDecoder(resp.Body).Decode(&newToken)
	if decodeErr != nil {
		log.WithError(decodeErr).Error("Cannot decode response")
	}

	log.WithFields(log.Fields{"token": newToken}).Info("Token updated")

	return newToken
}

func (api Shikimori) WhoAmI(token Token) ShikimoriUser {
	endpoint := BaseAPIUrl + "/users/whoami"
	resp := api.makeGetRequest(endpoint, token)

	var user ShikimoriUser
	err := json.NewDecoder(resp.Body).Decode(&user)
	if err != nil {
		log.WithError(err).Error("Cannot decode response")
	}

	return user
}

func (api Shikimori) GetNewChapters(token Token) []NewChapters {
	var newChapters []NewChapters
	var chapters []NewChapters
	for _, position := range api.getFullMangaList(token) {
		if "watching" == position.Status {
			chapters = position.GetNewChapters(token)
			if len(chapters) > 0 {
				newChapters = append(newChapters, chapters...)
			}

			// API access is limited by 5rps and 90rpm
			time.Sleep(ThrottlingTimeout)
		}
	}

	return newChapters
}

func (api Shikimori) getFullMangaList(token Token) []MangaPosition {
	endpoint := BaseAPIUrl + "/users/" + strconv.FormatInt(api.userID, 10) + "/manga_rates?page=1&limit=1000"
	resp := api.makeGetRequest(endpoint, token)

	var positions []MangaPosition
	err := json.NewDecoder(resp.Body).Decode(&positions)
	if err != nil {
		log.WithError(err).Error("Cannot decode response")
	}

	logFields := log.Fields{"total position": len(positions)}
	log.WithFields(logFields).Info("Manga list retrieved")

	return positions
}

func (api Shikimori) makeGetRequest(url string, token Token) *http.Response {
	req, reqErr := http.NewRequest("GET", url, nil)
	if reqErr != nil {
		log.WithError(reqErr).Error("Couldn't create a request")
	}

	req.Header.Add("Authorization", "Bearer "+token.AccessToken)
	req.Header.Add("User-Agent", UserAgent)

	client := &http.Client{}
	resp, respErr := client.Do(req)
	if respErr != nil {
		log.WithError(respErr).Error("Server replied with error")
	}

	return resp
}
