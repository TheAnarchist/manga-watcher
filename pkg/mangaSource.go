package pkg

import (
	"github.com/mmcdole/gofeed"
	log "github.com/sirupsen/logrus"
	"net/url"
	"regexp"
	"strconv"
	"time"
)

var weekAgo = time.Now().AddDate(0, 0, -7)

type MangaSourceInterface interface {
	RetrieveUnreadChapters(lastChapter int64) ([]gofeed.Item, error)
	getRssFeedItems() ([]*gofeed.Item, error)
}

type MangaSource struct {
	Id       int64  `json:"id"`
	Kind     string `json:"kind"`
	Url      string `json:"url"`
}

func (source MangaSource) RetrieveUnreadChapters(lastChapter int64) ([]gofeed.Item, error) {
	var newItems []gofeed.Item

	items, err := source.getRssFeedItems()
	if nil != err {
		return newItems, err
	}

	// filter: only new chapters that were released during the past week
	re := regexp.MustCompile(`[\d]+$`)
	for _, item := range items {
		curChapter, _ := strconv.ParseInt(re.FindString(item.GUID), 10, 64)
		if lastChapter >= curChapter || item.PublishedParsed.Before(weekAgo) {
			return newItems, nil
		}

		newItems = append(newItems, *item)
	}

	return newItems, nil
}

func (source MangaSource) getRssFeedItems() ([]*gofeed.Item, error) {
	var newItems []*gofeed.Item
	var err error

	u, err := url.Parse(source.Url)
	if nil != err {
		return newItems, err
	}

	rssUrl := u.Scheme + "://" + u.Host + "/rss/manga?name=" + GetCorrectTitle(u.Path)

	fp := gofeed.NewParser()
	feed, err := fp.ParseURL(rssUrl)
	if nil != err {
		return newItems, err
	}

	logFields := log.Fields{"title": feed.Title, "url": rssUrl, "total items": len(feed.Items)}
	log.WithFields(logFields).Info("RSS feed retrieved")

	return feed.Items, nil
}
