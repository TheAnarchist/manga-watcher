package pkg

import (
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"strings"
	"time"
)

const databaseTimeout = 10 * time.Second

type RepositoryInterface interface {
	FindUsersByWeekDay() []*User
	UpdateUserToken(userId primitive.ObjectID, newToken Token) bool
}

type Repository struct {
	context context.Context
	client  *mongo.Client
}

func (repo Repository) FindUsersByWeekDay() []*User {
	today := strings.ToLower(time.Now().Weekday().String())
	filter := bson.M{"weekday": today}

	var users []*User
	col := repo.client.Database(Database).Collection(Collection)
	cur, err := col.Find(repo.context, filter)
	if err != nil {
		log.WithError(err).Error("Error occurs: FindAllUsers")
	}

	defer cur.Close(context.Background())
	for cur.Next(context.Background()) {
		var user *User
		decodeErr := cur.Decode(&user)
		if decodeErr != nil {
			log.WithError(decodeErr).Error("Cannot decode user")
		}
		users = append(users, user)
	}

	return users
}

func (repo Repository) UpdateUserToken(userId primitive.ObjectID, newToken Token) bool {
	col := repo.client.Database(Database).Collection(Collection)

	result, err := col.UpdateOne(
		repo.context,
		bson.M{"_id": userId},
		bson.D{
			primitive.E{
				Key: "$set",
				Value: bson.D{
					primitive.E{
						Key:   "token",
						Value: newToken,
					},
				},
			},
		},
	)

	if err != nil {
		log.WithField("userId", userId).WithError(err).Error("Cannot update the user")
	}

	return result.ModifiedCount > 0
}
