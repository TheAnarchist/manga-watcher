package pkg

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/mmcdole/gofeed"
	log "github.com/sirupsen/logrus"
)

type MangaPositionInterface interface {
	GetNewChapters(token Token) []NewChapters
	GetExternalSources(token Token) []MangaSource
}

type MangaPosition struct {
	Id       int64  `json:"id"`
	Status   string `json:"status"`
	Chapters int64  `json:"chapters"`
	Manga    Manga
}

type Manga struct {
	Id            int64  `json:"id"`
	Status        string `json:"status"`
	Name          string `json:"name"`
	Russian       string `json:"russian"`
	ExternalLinks []MangaSource
}

type NewChapters struct {
	TotalNewChapters   int
	FirstUnreadChapter gofeed.Item
}

func (position MangaPosition) GetNewChapters(token Token) []NewChapters {
	var newChapters []NewChapters

	for _, source := range position.GetExternalSources(token) {
		if "readmanga" == source.Kind {
			newItems, err := source.RetrieveUnreadChapters(position.Chapters)
			if err != nil {
				errMsg := fmt.Sprintf("Couldn't retrieve new chapters for title: %s", position.Manga.Name)
				log.WithError(err).Error(errMsg)
			}

			if len(newItems) > 0 {
				newChapter := NewChapters{
					TotalNewChapters:   len(newItems),
					FirstUnreadChapter: newItems[len(newItems)-1],
				}

				newChapters = append(newChapters, newChapter)

				logFields := log.Fields{"title": position.Manga.Name, "number of new chapters": len(newItems)}
				log.WithFields(logFields).Info("New chapters found")
			}
		}
	}

	fields := log.Fields{"number of new chapters": len(newChapters)}
	log.WithFields(fields).Info("Total new chapters")

	return newChapters
}

func (position MangaPosition) GetExternalSources(token Token) []MangaSource {
	endpoint := fmt.Sprintf(BaseAPIUrl+"/mangas/%d/external_links", position.Manga.Id)
	req, reqErr := http.NewRequest("GET", endpoint, nil)
	if reqErr != nil {
		log.WithError(reqErr).Error("Error during creating a request to external api")
	}
	req.Header.Add("Authorization", "Bearer "+token.AccessToken)

	client := &http.Client{}
	resp, respErr := client.Do(req)
	if respErr != nil {
		log.WithError(respErr).Error("Error on calling external api")
	}

	var externalSources []MangaSource
	json.NewDecoder(resp.Body).Decode(&externalSources)

	return externalSources
}
