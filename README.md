# Manga watcher

I am a manga and anime fan. 

Some manga I am reading is ongoing. To track my progress I use [Shikimori](https://shikimori.one/) service (like [MyAnimeList](https://myanimelist.net/) but in Russian). And every week (usually weekend) I go through the list of ongoing manga and check if the title has a new chapter.

This small app helps me to check my manga list automatically.

Many thanks to beautiful [documentation](https://shikimori.one/api/doc/2.0).

### Technical stack:
- Go (v1.14)
- MongoDB Cloud (MongoDB as Service - [MongoDB Cloud](https://www.mongodb.com/cloud))
- Gitlab-CI (with [Pipeline Schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html))
    - Command runs every day at 8am (Berlin time): 0 8 * * *
    
### How to run:
> project has to be included in $GO_PATH
 
1. Clone a repo (with ssh): ```git clone git@gitlab.com:TheAnarchist/manga-watcher.git```
2. Go to the project folder: ```cd manga-watcher```
3. Install dependencies
4. Run: ```go run main.go```
