module gitlab.com/TheAnarchist/manga-watcher

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.4.0 // indirect
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
)
